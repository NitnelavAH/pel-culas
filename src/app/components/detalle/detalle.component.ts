import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { PeliculaDetalle, Actores } from '../../interfaces/interfaces';
import { ModalController, LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {

  @Input() id;

  pelicula: PeliculaDetalle = {};
  actores: Actores[] = [];
  oculto = 150;

  star = 'star-outline';

  slideOptActores = {
    slidesPerView: 3.3,
    freeMode: true,
    spacebetween: -5
  };

  constructor( private moviesService: MoviesService,
               private modalController: ModalController,
               private storage: StorageService
               ) { }

  ngOnInit() {

    this.storage.existePelicula( this.id ).then(existe => {
      this.star = (existe) ? 'star' : 'star-outline';
    });

    this.moviesService.getPeliculaDetalle( this.id ).subscribe((resp) => {
      this.pelicula = resp;
    });

    this.moviesService.getActoresPelicula( this.id ).subscribe((resp) => {
      this.actores = resp.cast;
    });

  }

  regresar() {
    this.modalController.dismiss();
  }

  favorito() {
    const existe = this.storage.guardarPelicula( this.pelicula );
    this.star = (existe) ? 'star' : 'star-outline';
  }

  

}
