import { Component } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { Pelicula } from 'src/app/interfaces/interfaces';
import { MoviesService } from 'src/app/services/movies.service';
import { DetalleComponent } from 'src/app/components/detalle/detalle.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  textoBuscar = '';

  ideas: string[] = ['Star Wars', 'Rápidos y Furiosos', 'Shrek', 'SpiderMan', 'Capitán América',
  'Iron Man', 'Toy Story', 'Resident Evil', 'Forrest Gump', '300', 'Avatar', 'El señor de los anillos'];

  peliculas: Pelicula[] = [];

  buscando = false;

  constructor(private moviesService: MoviesService, private modalCtrl: ModalController) {}

  buscar( event ) {
    const valor = event.detail.value;
    this.buscando = true;
    console.log(valor);
    if ( valor ){
      this.moviesService.buscarPelicula(valor).subscribe((resp) => {
        console.log(resp);
        this.peliculas = resp['results'];
      });
    } else {
      this.peliculas = [];
    }
    this.buscando = false;
  }

  async verDetalle( id: string ){
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: {
        id
      }
    });

    modal.present();
  }

}
