import { Component, OnInit } from '@angular/core';
import { PeliculaDetalle, Genre } from '../../interfaces/interfaces';
import { StorageService } from '../../services/storage.service';
import { MoviesService } from '../../services/movies.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  peliculas: PeliculaDetalle[] = [];

  generos: Genre[] = [];

  favoritoGenero: any[] = [];

  sliderOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };

  constructor(
    private storage: StorageService,
    private moviesservice: MoviesService,
    private router: Router,
    private loadingController: LoadingController
  ) {}


  ionViewWillEnter() {
    this.presentLoading();
    this.cargarDatos();
  }

  async cargarDatos(event?) {
    this. peliculas = await this.storage.cargarFavoritos();
    this.generos = await this.moviesservice.cargarGeneros();
    this.peliculasPorGenero(this.generos, this.peliculas);

  }

  peliculasPorGenero( generos: Genre[], peliculas: PeliculaDetalle[]){
    this.favoritoGenero = [];

    generos.forEach( genero => {
      this.favoritoGenero.push({
        genero: genero.name,
        peliculas: peliculas.filter( pelicula => {
          return pelicula.genres.find( genre => genre.id === genero.id);
        } )
      });
    });

  }

  redireccionar(){
    this.router.navigate(['/acercade']);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: '',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
}
